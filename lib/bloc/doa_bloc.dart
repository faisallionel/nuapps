import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:nuapps/models/models.dart';
import 'package:nuapps/services/services.dart';

part 'doa_event.dart';
part 'doa_state.dart';

class DoaBloc extends Bloc<DoaEvent, DoaState> {
  DoaBloc() : super(DoaInitial());

  @override
  Stream<DoaState> mapEventToState(
    DoaEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is FetchDoa) {
      Doa doa = await DoaServices.getDoaAll();

      yield DoaLoaded(doa);
    }
  }
}
