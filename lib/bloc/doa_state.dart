part of 'doa_bloc.dart';

abstract class DoaState extends Equatable {
  const DoaState();
}

class DoaInitial extends DoaState {
  @override
  List<Object> get props => [];
}

class DoaLoaded extends DoaState {
  final Doa doa;
  DoaLoaded(this.doa);
  @override
  List<Object> get props => [];
}
