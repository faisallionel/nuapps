import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:nuapps/models/models.dart';
part 'page_event.dart';
part 'page_state.dart';

class PageBloc extends Bloc<PageEvent, PageState> {
  PageBloc() : super(OnInitialPage());

  @override
  Stream<PageState> mapEventToState(
    PageEvent event,
  ) async* {
    if (event is GoToSplashPage) {
      yield OnSplashPage();
    } else if (event is GoToLoginPage) {
      yield OnLoginPage();
    } else if (event is GoToMainPage) {
      yield OnMainPage(pageIndex: event.pageIndex);
    } else if (event is GoToBeritaPage) {
      yield OnBeritaPage();
    } else if (event is GoToNewsDetailPage) {
      yield OnNewsDetailPage(event.news);
    } else if (event is GoToQuranDetailPage) {
      yield OnQuranDetailPage(event.dataQuran, event.detail);
    } else if (event is GoToQuranPage) {
      yield OnQuranPage();
    } else if (event is GoToNewsDetailPage) {
      yield OnNewsDetailPage(event.news);
    }
  }
}
