import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:nuapps/models/models.dart';
import 'package:nuapps/services/services.dart';

part 'quran_event.dart';
part 'quran_state.dart';

class QuranBloc extends Bloc<QuranEvent, QuranState> {
  QuranBloc() : super(QuranInitial());

  @override
  Stream<QuranState> mapEventToState(
    QuranEvent event,
  ) async* {
    if (event is FetchQuran) {
      Quran quran = await QuranServices.getQuranAll();

      yield QuranLoaded(quran: quran);
    }
  }
}
