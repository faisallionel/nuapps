part of 'quran_bloc.dart';

abstract class QuranEvent extends Equatable {
  const QuranEvent();
}

class FetchQuran extends QuranEvent {
  @override
  List<Object> get props => [];
}
