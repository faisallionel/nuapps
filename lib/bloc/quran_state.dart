part of 'quran_bloc.dart';

abstract class QuranState extends Equatable {
  const QuranState();
}

class QuranInitial extends QuranState {
  @override
  List<Object> get props => [];
}

class QuranLoaded extends QuranState {
  final Quran quran;
  QuranLoaded({this.quran});
  @override
  List<Object> get props => [];
}
