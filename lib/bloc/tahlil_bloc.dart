import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:nuapps/models/models.dart';
import 'package:nuapps/services/services.dart';

part 'tahlil_event.dart';
part 'tahlil_state.dart';

class TahlilBloc extends Bloc<TahlilEvent, TahlilState> {
  TahlilBloc() : super(TahlilInitial());

  @override
  Stream<TahlilState> mapEventToState(
    TahlilEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is FetchTahlil) {
      Tahlil tahlil = await TahlilServices.getTahlilAll();

      yield TahlilLoaded(tahlil);
    }
  }
}
