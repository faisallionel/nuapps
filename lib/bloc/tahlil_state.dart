part of 'tahlil_bloc.dart';

abstract class TahlilState extends Equatable {
  const TahlilState();
}

class TahlilInitial extends TahlilState {
  @override
  List<Object> get props => [];
}

class TahlilLoaded extends TahlilState {
  final Tahlil tahlil;
  TahlilLoaded(this.tahlil);

  @override
  List<Object> get props => [];
}
