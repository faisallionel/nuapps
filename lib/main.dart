import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:nuapps/bloc/blocs.dart';
import 'package:nuapps/bloc/news_bloc.dart';
import 'package:nuapps/bloc/tahlil_bloc.dart';
import 'package:nuapps/ui/pages/pages.dart';

import 'bloc/blocs.dart';

void main() async {
  await DotEnv().load(".env");
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => PageBloc()),
        BlocProvider(create: (_) => NewsBloc()..add(FetchNews())),
        BlocProvider(create: (_) => QuranBloc()..add(FetchQuran())),
        BlocProvider(create: (_) => TahlilBloc()..add(FetchTahlil())),
        BlocProvider(create: (_) => DoaBloc()..add(FetchDoa())),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Wrapper(),
      ),
    );
  }
}
