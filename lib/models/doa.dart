part of 'models.dart';

Doa doaFromJson(String str) => Doa.fromJson(json.decode(str));

String doaToJson(Doa data) => json.encode(data.toJson());

class Doa {
  Doa({
    this.data,
  });

  final List<DataDoa> data;

  factory Doa.fromJson(Map<String, dynamic> json) => Doa(
        data: json["data"] == null
            ? null
            : List<DataDoa>.from(json["data"].map((x) => DataDoa.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataDoa {
  DataDoa({
    this.doaId,
    this.namaDoa,
    this.ayatDoa,
    this.arabText,
    this.indoText,
    this.bacaText,
    this.keterangan,
  });

  final int doaId;
  final String namaDoa;
  final String ayatDoa;
  final String arabText;
  final String indoText;
  final String bacaText;
  final String keterangan;

  factory DataDoa.fromJson(Map<String, dynamic> json) => DataDoa(
        doaId: json["doa_id"] == null ? null : json["doa_id"],
        namaDoa: json["nama_doa"] == null ? null : json["nama_doa"],
        ayatDoa: json["ayat_doa"] == null ? null : json["ayat_doa"],
        arabText: json["arab_text"] == null ? null : json["arab_text"],
        indoText: json["indo_text"] == null ? null : json["indo_text"],
        bacaText: json["baca_text"] == null ? null : json["baca_text"],
      );
  Map<String, dynamic> toJson() => {
        "doa_id": doaId == null ? null : doaId,
        "nama_doa": namaDoa == null ? null : namaDoa,
        "ayat_doa": ayatDoa == null ? null : ayatDoa,
        "arab_text": arabText == null ? null : arabText,
        "indo_text": indoText == null ? null : indoText,
        "baca_text": bacaText == null ? null : bacaText,
      };
}
