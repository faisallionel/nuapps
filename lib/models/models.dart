import 'dart:convert';

import 'package:equatable/equatable.dart';

part 'user.dart';
part 'news.dart';
part 'news_detail.dart';
part 'picture.dart';
part 'quran.dart';
part 'quran_detail.dart';
part 'doa.dart';
part 'tahlil.dart';
