part of 'models.dart';

// ignore: must_be_immutable
class News extends Equatable {
  int id;
  String kategori;
  String prefix;
  String title;
  String preview;
  String viewer;
  String publishDate;
  String publishDateText;
  String publishDateHome;
  String url;
  Map<String, Picture> picture;

  News({
    this.id,
    this.kategori,
    this.prefix,
    this.title,
    this.preview,
    this.viewer,
    this.publishDate,
    this.publishDateText,
    this.publishDateHome,
    this.url,
    this.picture,
  });

  factory News.fromJson(Map<String, dynamic> json) => News(
        id: json["id"],
        kategori: json["kategori"],
        prefix: json["prefix"] == null ? null : json["prefix"],
        title: json["title"],
        preview: json["preview"],
        viewer: json["viewer"],
        publishDate: json["publish_date"],
        publishDateText: json["publish_date_text"],
        publishDateHome: json["publish_date_home"],
        url: json["url"],
        picture: Map.from(json["picture"])
            .map((k, v) => MapEntry<String, Picture>(k, Picture.fromJson(v))),
      );

  @override
  List<Object> get props => [
        id,
        kategori,
        prefix,
        title,
        preview,
        viewer,
        publishDate,
        publishDateText,
        publishDateHome,
        url,
        picture,
      ];
}
