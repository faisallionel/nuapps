part of 'models.dart';

// ignore: must_be_immutable

NewsDetail newsDetailFromJson(String str) =>
    NewsDetail.fromJson(json.decode(str));

String newsDetailToJson(NewsDetail data) => json.encode(data.toJson());

class NewsDetail {
  NewsDetail({
    this.data,
    this.totalPage,
    this.currentPage,
    this.additionalData,
    this.message,
  });

  final Data data;
  final dynamic totalPage;
  final dynamic currentPage;
  final AdditionalData additionalData;
  final String message;

  factory NewsDetail.fromJson(Map<String, dynamic> json) {
    return NewsDetail(
      data: json["data"] == null ? null : Data.fromJson(json["data"]),
      totalPage: json["totalPage"],
      currentPage: json["currentPage"],
      additionalData: json["additionalData"] == null
          ? null
          : AdditionalData.fromJson(json["additionalData"]),
      message: json["message"] == null ? null : json["message"],
    );
  }

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
        "totalPage": totalPage,
        "currentPage": currentPage,
        "additionalData":
            additionalData == null ? null : additionalData.toJson(),
        "message": message == null ? null : message,
      };
}

class AdditionalData {
  AdditionalData({
    this.title,
    this.desc,
    this.keyword,
    this.image,
    this.imgIco,
    this.mUrl,
    this.datePublished,
  });

  final String title;
  final String desc;
  final String keyword;
  final String image;
  final String imgIco;
  final String mUrl;
  final String datePublished;

  factory AdditionalData.fromJson(Map<String, dynamic> json) => AdditionalData(
        title: json["title"] == null ? null : json["title"],
        desc: json["desc"] == null ? null : json["desc"],
        keyword: json["keyword"] == null ? null : json["keyword"],
        image: json["image"] == null ? null : json["image"],
        imgIco: json["img_ico"] == null ? null : json["img_ico"],
        mUrl: json["m_url"] == null ? null : json["m_url"],
        datePublished:
            json["datePublished"] == null ? null : json["datePublished"],
      );

  Map<String, dynamic> toJson() => {
        "title": title == null ? null : title,
        "desc": desc == null ? null : desc,
        "keyword": keyword == null ? null : keyword,
        "image": image == null ? null : image,
        "img_ico": imgIco == null ? null : imgIco,
        "m_url": mUrl == null ? null : mUrl,
        "datePublished": datePublished == null ? null : datePublished,
      };
}

class Data {
  Data({
    this.go,
    this.id,
    this.kategori,
    this.urlKategori,
    this.prefix,
    this.title,
    this.publishDate,
    this.publishDateText,
    this.updateDate,
    this.slug,
    this.url,
    this.tags,
    this.total,
    this.child,
    this.author,
    this.jumlahLinkage,
    this.linkage,
  });

  final int go;
  final int id;
  final String kategori;
  final String urlKategori;
  final dynamic prefix;
  final String title;
  final DateTime publishDate;
  final String publishDateText;
  final DateTime updateDate;
  final String slug;
  final String url;
  final List<String> tags;
  final int total;
  final Child child;
  final Author author;
  final int jumlahLinkage;
  final List<Linkage> linkage;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        go: json["go"] == null ? null : json["go"],
        id: json["id"] == null ? null : json["id"],
        kategori: json["kategori"] == null ? null : json["kategori"],
        urlKategori: json["url_kategori"] == null ? null : json["url_kategori"],
        prefix: json["prefix"],
        title: json["title"] == null ? null : json["title"],
        publishDate: json["publish_date"] == null
            ? null
            : DateTime.parse(json["publish_date"]),
        publishDateText: json["publish_date_text"] == null
            ? null
            : json["publish_date_text"],
        updateDate: json["update_date"] == null
            ? null
            : DateTime.parse(json["update_date"]),
        slug: json["slug"] == null ? null : json["slug"],
        url: json["url"] == null ? null : json["url"],
        tags: json["tags"] == null
            ? null
            : List<String>.from(json["tags"].map((x) => x)),
        total: json["total"] == null ? null : json["total"],
        child: json["child"] == null ? null : Child.fromJson(json["child"]),
        author: json["author"] == null ? null : Author.fromJson(json["author"]),
        jumlahLinkage:
            json["jumlah_linkage"] == null ? null : json["jumlah_linkage"],
        linkage: json["linkage"] == null
            ? null
            : List<Linkage>.from(
                json["linkage"].map((x) => Linkage.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "go": go == null ? null : go,
        "id": id == null ? null : id,
        "kategori": kategori == null ? null : kategori,
        "url_kategori": urlKategori == null ? null : urlKategori,
        "prefix": prefix,
        "title": title == null ? null : title,
        "publish_date":
            publishDate == null ? null : publishDate.toIso8601String(),
        "publish_date_text": publishDateText == null ? null : publishDateText,
        "update_date": updateDate == null ? null : updateDate.toIso8601String(),
        "slug": slug == null ? null : slug,
        "url": url == null ? null : url,
        "tags": tags == null ? null : List<dynamic>.from(tags.map((x) => x)),
        "total": total == null ? null : total,
        "child": child == null ? null : child.toJson(),
        "author": author == null ? null : author.toJson(),
        "jumlah_linkage": jumlahLinkage == null ? null : jumlahLinkage,
        "linkage": linkage == null
            ? null
            : List<dynamic>.from(linkage.map((x) => x.toJson())),
      };
}

class Author {
  Author({
    this.jumlahAuthor,
    this.data,
  });

  final int jumlahAuthor;
  final List<Datum> data;

  factory Author.fromJson(Map<String, dynamic> json) => Author(
        jumlahAuthor:
            json["jumlah_author"] == null ? null : json["jumlah_author"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "jumlah_author": jumlahAuthor == null ? null : jumlahAuthor,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.type,
    this.name,
    this.nickName,
    this.urlAuthor,
    this.picture,
  });

  final String type;
  final String name;
  final String nickName;
  final String urlAuthor;
  final String picture;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        type: json["type"] == null ? null : json["type"],
        name: json["name"] == null ? null : json["name"],
        nickName: json["nick_name"] == null ? null : json["nick_name"],
        urlAuthor: json["url_author"] == null ? null : json["url_author"],
        picture: json["picture"] == null ? null : json["picture"],
      );

  Map<String, dynamic> toJson() => {
        "type": type == null ? null : type,
        "name": name == null ? null : name,
        "nick_name": nickName == null ? null : nickName,
        "url_author": urlAuthor == null ? null : urlAuthor,
        "picture": picture == null ? null : picture,
      };
}

class Child {
  Child({
    this.picture,
    this.id,
    this.caption,
    this.content,
    this.page,
  });

  final Map<String, Picture> picture;
  final int id;
  final String caption;
  final String content;
  final int page;

  factory Child.fromJson(Map<String, dynamic> json) => Child(
        picture: json["picture"] == null
            ? null
            : Map.from(json["picture"]).map(
                (k, v) => MapEntry<String, Picture>(k, Picture.fromJson(v))),
        id: json["id"] == null ? null : json["id"],
        caption: json["caption"] == null ? null : json["caption"],
        content: json["content"] == null ? null : json["content"],
        page: json["page"] == null ? null : json["page"],
      );

  Map<String, dynamic> toJson() => {
        "picture": picture == null
            ? null
            : Map.from(picture)
                .map((k, v) => MapEntry<String, dynamic>(k, v.toJson())),
        "id": id == null ? null : id,
        "caption": caption == null ? null : caption,
        "content": content == null ? null : content,
        "page": page == null ? null : page,
      };
}

class Linkage {
  Linkage({
    this.id,
    this.kategori,
    this.title,
    this.url,
    this.picture,
  });

  final int id;
  final String kategori;
  final String title;
  final String url;
  final Map<String, Picture> picture;

  factory Linkage.fromJson(Map<String, dynamic> json) => Linkage(
        id: json["id"] == null ? null : json["id"],
        kategori: json["kategori"] == null ? null : json["kategori"],
        title: json["title"] == null ? null : json["title"],
        url: json["url"] == null ? null : json["url"],
        picture: json["picture"] == null
            ? null
            : Map.from(json["picture"]).map(
                (k, v) => MapEntry<String, Picture>(k, Picture.fromJson(v))),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "kategori": kategori == null ? null : kategori,
        "title": title == null ? null : title,
        "url": url == null ? null : url,
        "picture": picture == null
            ? null
            : Map.from(picture)
                .map((k, v) => MapEntry<String, dynamic>(k, v.toJson())),
      };
}
