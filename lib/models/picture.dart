part of 'models.dart';

// ignore: must_be_immutable
class Picture extends Equatable {
  String big;
  String mid;
  String thumb;

  Picture({
    this.big,
    this.mid,
    this.thumb,
  });

  factory Picture.fromJson(Map<String, dynamic> json) => Picture(
        big: json["big"],
        mid: json["mid"],
        thumb: json["thumb"],
      );

  @override
  List<Object> get props => [
        this.big,
        this.mid,
        this.thumb,
      ];
}
