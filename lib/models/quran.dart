part of 'models.dart';

Quran quranFromJson(String str) => Quran.fromJson(json.decode(str));

String quranToJson(Quran data) => json.encode(data.toJson());

class Quran {
  Quran({
    this.data,
    this.totalPage,
    this.currentPage,
    this.additionalData,
    this.message,
  });

  final List<DataQuran> data;
  final dynamic totalPage;
  final dynamic currentPage;
  final dynamic additionalData;
  final String message;

  factory Quran.fromJson(Map<String, dynamic> json) => Quran(
        data: json["data"] == null
            ? null
            : List<DataQuran>.from(
                json["data"].map((x) => DataQuran.fromJson(x))),
        totalPage: json["totalPage"],
        currentPage: json["currentPage"],
        additionalData: json["additionalData"],
        message: json["message"] == null ? null : json["message"],
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
        "totalPage": totalPage,
        "currentPage": currentPage,
        "additionalData": additionalData,
        "message": message == null ? null : message,
      };
}

class DataQuran {
  DataQuran({
    this.uuid,
    this.id,
    this.name,
    this.nameLatin,
  });

  final String uuid;
  final int id;
  final String name;
  final String nameLatin;

  factory DataQuran.fromJson(Map<String, dynamic> json) => DataQuran(
        uuid: json["uuid"] == null ? null : json["uuid"],
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        nameLatin: json["name_latin"] == null ? null : json["name_latin"],
      );

  Map<String, dynamic> toJson() => {
        "uuid": uuid == null ? null : uuid,
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "name_latin": nameLatin == null ? null : nameLatin,
      };
}
