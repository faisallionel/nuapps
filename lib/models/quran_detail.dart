part of 'models.dart';

QuranDetail quranDetailFromJson(String str) =>
    QuranDetail.fromJson(json.decode(str));

String quranDetailToJson(QuranDetail data) => json.encode(data.toJson());

class QuranDetail {
  QuranDetail({
    this.data,
    this.totalPage,
    this.currentPage,
    this.additionalData,
    this.message,
  });

  final DataQuranDetail data;
  final dynamic totalPage;
  final dynamic currentPage;
  final dynamic additionalData;
  final String message;

  factory QuranDetail.fromJson(Map<String, dynamic> json) => QuranDetail(
        data: json["data"] == null
            ? null
            : DataQuranDetail.fromJson(json["data"]),
        totalPage: json["totalPage"],
        currentPage: json["currentPage"],
        additionalData: json["additionalData"],
        message: json["message"] == null ? null : json["message"],
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? null : data.toJson(),
        "totalPage": totalPage,
        "currentPage": currentPage,
        "additionalData": additionalData,
        "message": message == null ? null : message,
      };
}

class DataQuranDetail {
  DataQuranDetail({
    this.number,
    this.name,
    this.nameLatin,
    this.numberOfAyah,
    this.text,
    this.translations,
    this.tafsir,
  });

  final String number;
  final String name;
  final String nameLatin;
  final String numberOfAyah;
  final Map<String, String> text;
  final Translations translations;
  final Tafsir tafsir;

  factory DataQuranDetail.fromJson(Map<String, dynamic> json) =>
      DataQuranDetail(
        number: json["number"] == null ? null : json["number"],
        name: json["name"] == null ? null : json["name"],
        nameLatin: json["name_latin"] == null ? null : json["name_latin"],
        numberOfAyah:
            json["number_of_ayah"] == null ? null : json["number_of_ayah"],
        text: json["text"] == null
            ? null
            : Map.from(json["text"])
                .map((k, v) => MapEntry<String, String>(k, v)),
        translations: json["translations"] == null
            ? null
            : Translations.fromJson(json["translations"]),
        tafsir: json["tafsir"] == null ? null : Tafsir.fromJson(json["tafsir"]),
      );

  Map<String, dynamic> toJson() => {
        "number": number == null ? null : number,
        "name": name == null ? null : name,
        "name_latin": nameLatin == null ? null : nameLatin,
        "number_of_ayah": numberOfAyah == null ? null : numberOfAyah,
        "text": text == null
            ? null
            : Map.from(text).map((k, v) => MapEntry<String, dynamic>(k, v)),
        "translations": translations == null ? null : translations.toJson(),
        "tafsir": tafsir == null ? null : tafsir.toJson(),
      };
}

class Tafsir {
  Tafsir({
    this.id,
  });

  final TafsirId id;

  factory Tafsir.fromJson(Map<String, dynamic> json) => Tafsir(
        id: json["id"] == null ? null : TafsirId.fromJson(json["id"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id.toJson(),
      };
}

class TafsirId {
  TafsirId({
    this.kemenag,
  });

  final Kemenag kemenag;

  factory TafsirId.fromJson(Map<String, dynamic> json) => TafsirId(
        kemenag:
            json["kemenag"] == null ? null : Kemenag.fromJson(json["kemenag"]),
      );

  Map<String, dynamic> toJson() => {
        "kemenag": kemenag == null ? null : kemenag.toJson(),
      };
}

class Kemenag {
  Kemenag({
    this.name,
    this.source,
    this.text,
  });

  final String name;
  final String source;
  final Map<String, String> text;

  factory Kemenag.fromJson(Map<String, dynamic> json) => Kemenag(
        name: json["name"] == null ? null : json["name"],
        source: json["source"] == null ? null : json["source"],
        text: json["text"] == null
            ? null
            : Map.from(json["text"])
                .map((k, v) => MapEntry<String, String>(k, v)),
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "source": source == null ? null : source,
        "text": text == null
            ? null
            : Map.from(text).map((k, v) => MapEntry<String, dynamic>(k, v)),
      };
}

class Translations {
  Translations({
    this.id,
  });

  final TranslationsId id;

  factory Translations.fromJson(Map<String, dynamic> json) => Translations(
        id: json["id"] == null ? null : TranslationsId.fromJson(json["id"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id.toJson(),
      };
}

class TranslationsId {
  TranslationsId({
    this.name,
    this.text,
  });

  final String name;
  final Map<String, String> text;

  factory TranslationsId.fromJson(Map<String, dynamic> json) => TranslationsId(
        name: json["name"] == null ? null : json["name"],
        text: json["text"] == null
            ? null
            : Map.from(json["text"])
                .map((k, v) => MapEntry<String, String>(k, v)),
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "text": text == null
            ? null
            : Map.from(text).map((k, v) => MapEntry<String, dynamic>(k, v)),
      };
}
