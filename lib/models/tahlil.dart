part of 'models.dart';

Tahlil tahlilFromJson(String str) => Tahlil.fromJson(json.decode(str));

String tahlilToJson(Tahlil data) => json.encode(data.toJson());

class Tahlil {
  Tahlil({
    this.data,
  });

  final List<DataTahlil> data;

  factory Tahlil.fromJson(Map<String, dynamic> json) => Tahlil(
        data: json["data"] == null
            ? null
            : List<DataTahlil>.from(
                json["data"].map((x) => DataTahlil.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataTahlil {
  DataTahlil({
    this.ayatDoa,
    this.arabText,
    this.indoText,
  });

  final int ayatDoa;
  final String arabText;
  final String indoText;

  factory DataTahlil.fromJson(Map<String, dynamic> json) => DataTahlil(
        ayatDoa: json["ayat_doa"] == null ? null : json["ayat_doa"],
        arabText: json["arab_text"] == null ? null : json["arab_text"],
        indoText: json["indo_text"] == null ? null : json["indo_text"],
      );

  Map<String, dynamic> toJson() => {
        "ayat_doa": ayatDoa == null ? null : ayatDoa,
        "arab_text": arabText == null ? null : arabText,
        "indo_text": indoText == null ? null : indoText,
      };
}
