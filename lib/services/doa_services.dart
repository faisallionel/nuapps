part of 'services.dart';

class DoaServices {
  static Future<Doa> getDoaAll({http.Client client}) async {
    String url = apiDoa;
    client ??= http.Client();

    var res = await client.get(url);
    Map<String, dynamic> data = json.decode(res.body);

    return Doa.fromJson(data);
  }

  static Future<QuranDetail> getDetail(DataQuran quran,
      {http.Client client}) async {
    String url = apiQuranDetail + quran.id.toString();

    client ??= http.Client();

    var res = await client.get(url);

    Map<String, dynamic> data = json.decode(res.body);

    return QuranDetail.fromJson(data);
  }

  static Future<String> getNameSurah(DataQuran quran,
      {http.Client client}) async {
    String url = apiQuranDetail + quran.id.toString();

    client ??= http.Client();

    var res = await client.get(url);

    // Map<String, dynamic> data = json.decode(res.body);
    Map<String, dynamic> data = json.decode(utf8.decode(res.bodyBytes));
    return QuranDetail.fromJson(data).data.nameLatin +
        " (" +
        QuranDetail.fromJson(data).data.translations.id.name +
        ") ";
  }
}
