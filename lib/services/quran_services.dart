part of 'services.dart';

class QuranServices {
  static Future<Quran> getQuranAll({http.Client client}) async {
    String url = "https://ardalabs.nu.or.id/api/v1/quran/all";

    client ??= http.Client();

    var res = await client.get(url);
    Map<String, dynamic> data = json.decode(res.body);

    print("========ini list quran");
    print(res);

    return Quran.fromJson(data);
  }

  static Future<QuranDetail> getDetail(DataQuran quran,
      {http.Client client}) async {
    String url = apiQuranDetail + quran.id.toString();

    client ??= http.Client();

    var res = await client.get(url);

    Map<String, dynamic> data = json.decode(res.body);

    return QuranDetail.fromJson(data);
  }

  static Future<String> getNameSurah(DataQuran quran,
      {http.Client client}) async {
    String url = apiQuranDetail + quran.id.toString();

    client ??= http.Client();

    var res = await client.get(url);

    // Map<String, dynamic> data = json.decode(res.body);
    Map<String, dynamic> data = json.decode(utf8.decode(res.bodyBytes));
    return QuranDetail.fromJson(data).data.nameLatin +
        " (" +
        QuranDetail.fromJson(data).data.translations.id.name +
        ") ";
  }
}
