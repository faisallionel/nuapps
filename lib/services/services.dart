import 'package:nuapps/models/models.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:nuapps/shared/shared.dart';

part 'news_services.dart';
part 'quran_services.dart';
part 'doa_services.dart';
part 'tahlil_services.dart';

// PLEASE SEE AT THE HOST IN .ENV FILE AND SHARED.DART FILES
