part of 'services.dart';

class TahlilServices {
  static Future<Tahlil> getTahlilAll({http.Client client}) async {
    String url = apiTahlil;

    client ??= http.Client();

    var res = await client.get(url);
    Map<String, dynamic> data = json.decode(res.body);
    print("========ini response");
    print(res.body);

    return Tahlil.fromJson(data);
  }

  static Future<QuranDetail> getDetail(DataQuran quran,
      {http.Client client}) async {
    String url = apiQuranDetail + quran.id.toString();

    client ??= http.Client();

    var res = await client.get(url);

    Map<String, dynamic> data = json.decode(res.body);

    return QuranDetail.fromJson(data);
  }

  static Future<String> getNameSurah(DataQuran quran,
      {http.Client client}) async {
    String url = apiQuranDetail + quran.id.toString();

    client ??= http.Client();

    var res = await client.get(url);

    // Map<String, dynamic> data = json.decode(res.body);
    Map<String, dynamic> data = json.decode(utf8.decode(res.bodyBytes));
    return QuranDetail.fromJson(data).data.nameLatin +
        " (" +
        QuranDetail.fromJson(data).data.translations.id.name +
        ") ";
  }
}
