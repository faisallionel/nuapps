import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:nuapps/bloc/blocs.dart';

part 'shared_value.dart';
part 'theme.dart';
part 'sizing.dart';
