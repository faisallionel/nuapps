part of 'shared.dart';

String apiKey = "1531346421784462eabb05e5cf480269";
String imageBaseURL = "https://image.tmdb.org/t/p/";

PageEvent prevPageEvent;

String hostArda = DotEnv().env['HOST_ARDA_DEV'];
String hostNu = DotEnv().env['HOST_NU_DEV'];

String apiNewsDetail = hostArda + "/post/detail/";
String apiQuranDetail = hostArda + "/quran/detail/";
String apiDoa = hostNu + "/doa";
String apiTahlil = hostNu + "/tahlil";
