part of 'shared.dart';

class SizeConfig {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double blockSizeHorizontal;
  static double blockSizeVertical;
  static double _safeAreaHorizontal;
  static double _safeAreaVertical;
  static double safeBlockHorizontal;
  static double safeBlockVertical;

  MediaQueryData get queryData => _mediaQueryData;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    blockSizeHorizontal = screenWidth / 100;
    blockSizeVertical = screenHeight / 100;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    safeBlockHorizontal = (screenWidth - _safeAreaHorizontal) / 100;
    safeBlockVertical = (screenHeight - _safeAreaVertical) / 100;
  }
}

// SPLASH SCREEN
class TitleSize {
  static const top = 550;
  static const right = 280;
  static const left = 250;
}

class DescSize {
  static const top = 502;
  static const right = 380;
  static const left = 300;
}

class SecondDescSize {
  static const top = 480;
  static const right = 380;
  static const left = 270;
}

class BgSize {
  static const top = 420;
  static const right = 300;
  static const left = 300;
}

class BtnSize {
  static const top = 110;
  static const right = 250;
  static const left = 255;
}

class LogoSize {
  static const top = 300;
  static const right = 250;
  static const left = 255;
}

class StringConst {
  static const APP_NAME = "NU APPS";
  static const APP_DESC = "Sejenak lebih dekat dengan Ilahi ";
  static const APP_DESC_LINE_2 = "melalui aplikasi NU digital ";
  static const APP_START = "Mulai";
  static const SEMI_FONT_NAME = 'MaisonNeue';
  static const BOLD_FONT_NAME = 'MaisonNeue bold';
  static const RECENT_NEWS = "Berita Terbaru";
  static const ALL_NEWS = "Semua Berita ";
}
