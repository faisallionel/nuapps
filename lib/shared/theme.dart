part of 'shared.dart';

Color mainColor = Color(0xFF503E9D);
Color accentColor1 = Color(0xFF503E9D);
Color accentColor2 = Color(0xFFFBD460);
Color accentColor3 = Color(0xFFADADAD);

TextStyle blackTextFont = GoogleFonts.roboto().copyWith(
    color: Hexcolor("#038175"), fontWeight: FontWeight.bold, fontSize: 30);
TextStyle whiteTextFont = GoogleFonts.raleway()
    .copyWith(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18);
TextStyle purpleTextFont = GoogleFonts.raleway()
    .copyWith(color: mainColor, fontWeight: FontWeight.w500);
TextStyle greyTextFont = GoogleFonts.raleway()
    .copyWith(color: accentColor1, fontWeight: FontWeight.w500);

TextStyle whiteNumbeFont = GoogleFonts.raleway()
    .copyWith(color: accentColor1, fontWeight: FontWeight.w500);

TextStyle maisonNeueNews1 = TextStyle(
    fontFamily: "MaisonNeue bold",
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.bold,
    fontSize: SizeConfig.safeBlockHorizontal * 6,
    color: Hexcolor("#000000"));
TextStyle maisonNeueNews2 = TextStyle(
    fontFamily: "MaisonAjai",
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w300,
    fontSize: SizeConfig.safeBlockHorizontal * 4,
    color: Color.fromRGBO(3, 129, 117, 0.5));
TextStyle maisonNeueNews3 = TextStyle(
    fontFamily: "MaisonAjai",
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w300,
    fontSize: SizeConfig.safeBlockHorizontal * 4,
    color: Hexcolor("#B6BDBC"));
TextStyle maisonNeueNews4 = TextStyle(
    fontFamily: "MaisonAjai",
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w100,
    fontSize: SizeConfig.safeBlockHorizontal * 4,
    color: Colors.black);
TextStyle maisonNeueNewsLink1 = TextStyle(
    fontFamily: "MaisonAjai",
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w100,
    fontSize: SizeConfig.safeBlockHorizontal * 3,
    color: Colors.black);
TextStyle maisonNeueNewsLink2 = TextStyle(
    fontFamily: "MaisonAjai",
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.w100,
    fontSize: SizeConfig.safeBlockHorizontal * 3,
    color: Color.fromRGBO(3, 129, 117, 0.5));
