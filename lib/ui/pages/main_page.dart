part of 'pages.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();

  final int pageIndex;
  final String pageName;
  MainPage(this.pageIndex, this.pageName);
}

class _MainPageState extends State<MainPage> {
  int pageIndex;
  String pageName;
  PageController pageController;

  static List<Widget> _widgetOptions = <Widget>[
    NewsWidgets(),
    QuranWidgets(),
    DoaWidgets(),
    TahlilWidgets(),
  ];

  @override
  void initState() {
    super.initState();

    pageIndex = widget.pageIndex;
    pageName = widget.pageName;
    pageController = PageController(initialPage: pageIndex);
  }

  @override
  Widget build(BuildContext context) {
    void _onItemTapped(int index) {
      setState(() {
        pageIndex = index;
      });
      if (index == 0) {
        setState(() {
          pageName = "Berita";
        });
      } else if (index == 1) {
        setState(() {
          pageName = "Quran";
        });
      } else if (index == 2) {
        setState(() {
          pageName = "Doa";
        });
      } else {
        setState(() {
          pageName = "Tahlil";
        });
      }
    }

    MediaQueryData queryData;
    queryData = MediaQuery.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(pageName),
        backgroundColor: Hexcolor("#038175"),
        centerTitle: true,
      ),
      body: Stack(
        children: [
          Container(
            height: queryData.size.height,
            width: queryData.size.width,
            child: _widgetOptions.elementAt(pageIndex),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Hexcolor("#E8F6EF"),
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Tab(
                icon: new Icon(
              Icons.library_books,
              color: Colors.black,
            )),
            title: Text('News'),
          ),
          BottomNavigationBarItem(
            icon: Tab(
              icon: new Image.asset(
                "assets/icons/quran.png",
              ),
            ),
            title: Text('Quran'),
          ),
          BottomNavigationBarItem(
            icon: Tab(
              icon: new Image.asset(
                "assets/icons/doa.png",
              ),
            ),
            title: Text('Doa'),
          ),
          BottomNavigationBarItem(
            icon: Tab(
              icon: new Image.asset(
                "assets/icons/tahlil.png",
              ),
            ),
            title: Text('Tahlil'),
          ),
        ],
        currentIndex: pageIndex,
        selectedItemColor: Hexcolor("#038175"),
        onTap: _onItemTapped,
      ),
    );
  }
}
