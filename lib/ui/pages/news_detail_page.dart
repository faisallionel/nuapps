part of 'pages.dart';

class NewsDetailPage extends StatelessWidget {
  final News news;

  NewsDetailPage(this.news);

  @override
  Widget build(BuildContext context) {
    NewsDetail newsDetail;

    MediaQueryData queryData;
    queryData = MediaQuery.of(context);

    SizeConfig().init(context);
    return WillPopScope(
        onWillPop: () async {
          context.bloc<PageBloc>().add(GoToMainPage(pageIndex: 0));
          return false;
        },
        child: Scaffold(
            appBar: AppBar(
                title: Text("Detail Berita"),
                backgroundColor: Hexcolor("#038175"),
                centerTitle: true,
                leading: new IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    context.bloc<PageBloc>().add(GoToMainPage(pageIndex: 0));
                  },
                )),
            body: FutureBuilder(
                future: NewsServices.getDetail(news),
                builder: (_, snapshot) {
                  if (snapshot.hasData) {
                    newsDetail = snapshot.data;
                    return WidgetsNewsDetail(news: newsDetail);
                  } else {
                    return SpinKitFadingCircle(
                      color: mainColor,
                      size: queryData.size.width * .5,
                    );
                  }
                })));
  }
}

class WidgetsNewsDetail extends StatelessWidget {
  final NewsDetail news;

  WidgetsNewsDetail({this.news});
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.safeBlockHorizontal * 5,
          vertical: SizeConfig.safeBlockVertical * 4),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: SizeConfig.blockSizeVertical * 30,
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.safeBlockHorizontal * 3,
                  vertical: SizeConfig.safeBlockVertical * 3),
              child: FadeInImage.memoryNetwork(
                placeholder: kTransparentImage,
                image: news.additionalData.image,
              ),
            ),
          ),
          Text(
            news.data.title,
            style: maisonNeueNews1,
          ),
          Row(children: <Widget>[
            Expanded(
              child: Text(
                news.data.publishDateText,
                style: maisonNeueNews2,
              ),
              flex: 12,
            ),
            Expanded(
              child: Icon(Icons.show_chart),
              flex: 2,
            ),
            Expanded(
              child: Text("55"),
              flex: 1,
            )
          ]),
          Text(
            "Kategori : " + news.data.kategori,
            style: maisonNeueNews3,
          ),
          SizedBox(
            height: SizeConfig.safeBlockVertical * 3,
          ),
          Html(
            data: news.data.child.content,
          ),
          // RichText(
          //   text: TextSpan(style: maisonNeueNews4, children: <TextSpan>[
          //     TextSpan(text: news.data.slug, style: maisonNeueNews3),
          //   TextSpan(text: " - " + news.data.child.content)
          //   ]),
          //   textAlign: TextAlign.justify,
          // ),
          SizedBox(
            height: SizeConfig.safeBlockVertical * 4,
          ),
          Text(
            "sumber : ",
            style: maisonNeueNewsLink1,
            textAlign: TextAlign.justify,
          ),
          RichText(
            text: TextSpan(children: [
              TextSpan(
                  text: news.additionalData.mUrl,
                  style: maisonNeueNewsLink2,
                  recognizer: new TapGestureRecognizer()
                    ..onTap = () {
                      launch(news.additionalData.mUrl);
                    })
            ]),
          ),
        ],
      ),
    );
  }
}
