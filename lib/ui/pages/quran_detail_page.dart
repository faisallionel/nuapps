part of 'pages.dart';

class QuranDetailPage extends StatelessWidget {
  final DataQuran quran;
  final QuranDetail detail;

  QuranDetailPage(this.quran, this.detail);

  @override
  Widget build(BuildContext context) {
    QuranDetail quranDetail;

    MediaQueryData queryData;
    queryData = MediaQuery.of(context);

    SizeConfig().init(context);
    String _surahName = "Detail Quran";
    return WillPopScope(
        onWillPop: () async {
          // Navigator.pop(context);
          context.bloc<PageBloc>().add(GoToMainPage(pageIndex: 1));
          return false;
        },
        child: Scaffold(
            appBar: AppBar(
                title: new FutureBuilder<String>(
                  future: QuranServices.getNameSurah(quran),
                  builder: (_, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Text("Detail Surah");
                    } else {
                      return Text(snapshot.data);
                    }
                  },
                ),
                backgroundColor: Hexcolor("#038175"),
                centerTitle: true,
                leading: new IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    context.bloc<PageBloc>().add(GoToMainPage(pageIndex: 1));
                  },
                )),
            body: FutureBuilder(
                future: QuranServices.getDetail(quran),
                builder: (_, snapshot) {
                  if (snapshot.hasData) {
                    QuranDetail quranDetail = snapshot.data;
                    return QuranDetailCards(
                      detail: quranDetail,
                      quran: quran,
                    );
                  } else {
                    return SpinKitFadingCircle(
                      color: mainColor,
                      size: queryData.size.width * .5,
                    );
                  }
                })));
  }
}

class QuranDetailCards extends StatelessWidget {
  final DataQuran quran;
  final QuranDetail detail;

  QuranDetailCards({this.quran, this.detail});
  List<String> text = List();
  List<String> tafsir = List();
  List<String> translation = List();

  @override
  // ignore: missing_return
  Widget build(BuildContext context) {
    detail.data.text.forEach((k, v) {
      text.add(v);
    });
    detail.data.tafsir.id.kemenag.text.forEach((k, v) {
      tafsir.add(v);
    });
    detail.data.translations.id.text.forEach((k, v) {
      translation.add(v);
    });

    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    SizeConfig().init(context);
    return ListView.builder(
      itemCount: text.length,
      shrinkWrap: true,
      itemBuilder: (BuildContext context, int index) {
        return ExpansionTile(
          leading: Container(
            width: SizeConfig.safeBlockHorizontal * 13,
            height: SizeConfig.safeBlockHorizontal * 13,
            decoration: BoxDecoration(
              color: Hexcolor("#038175"),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            child: Padding(
              padding: EdgeInsets.fromLTRB(
                  SizeConfig.safeBlockHorizontal * 5,
                  SizeConfig.safeBlockHorizontal * 4,
                  SizeConfig.safeBlockHorizontal * 0,
                  SizeConfig.safeBlockHorizontal * 0),
              child: Text(
                (index + 1).toString(),
                overflow: TextOverflow.ellipsis,
                style: GoogleFonts.roboto().copyWith(
                  color: Colors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ),
          ),
          title: Wrap(
            crossAxisAlignment: WrapCrossAlignment.end,
            alignment: WrapAlignment.end,
            children: [
              Text(
                text[index],
                textAlign: TextAlign.right,
                overflow: TextOverflow.visible,
                style: new TextStyle(
                    fontFamily: ArabicFonts.Scheherazade,
                    package: 'google_fonts_arabic',
                    fontSize: SizeConfig.safeBlockHorizontal * 8,
                    color: Hexcolor("#038175")),
              ),
            ],
          ),
          subtitle: Row(
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Text(
                    translation[index],
                    style: new TextStyle(
                        fontFamily: ArabicFonts.Cairo,
                        package: 'google_fonts_arabic',
                        fontSize: SizeConfig.safeBlockHorizontal * 3,
                        color: Hexcolor("#034f81")),
                    textAlign: TextAlign.right,
                    overflow: TextOverflow.visible,
                  )),
            ],
          ),
          children: <Widget>[
            Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.safeBlockHorizontal * 10),
                child: Text(tafsir[index]))
          ],
        );
      },
    );
  }
}
