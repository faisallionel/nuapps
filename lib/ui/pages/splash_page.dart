part of 'pages.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Stack(
          children: <Widget>[
            Positioned(
                top: queryData.size.height - TitleSize.top,
                right: queryData.size.width - TitleSize.right,
                left: queryData.size.width - TitleSize.left,
                child: Text(
                  StringConst.APP_NAME,
                  style: TextStyle(
                      fontFamily: StringConst.BOLD_FONT_NAME,
                      color: Color(0xff038175),
                      fontWeight: FontWeight.bold,
                      fontSize: 30),
                )),
            Positioned(
                top: queryData.size.height - DescSize.top,
                right: queryData.size.width - DescSize.right,
                left: queryData.size.width - DescSize.left,
                child: Text(
                  StringConst.APP_DESC,
                  style: TextStyle(
                      fontFamily: StringConst.SEMI_FONT_NAME,
                      color: Color(0xffB6BDBC),
                      fontSize: 18),
                )),
            Positioned(
                top: queryData.size.height - SecondDescSize.top,
                right: queryData.size.width - SecondDescSize.right,
                left: queryData.size.width - SecondDescSize.left,
                child: Text(
                  StringConst.APP_DESC_LINE_2,
                  style: TextStyle(
                      fontFamily: StringConst.SEMI_FONT_NAME,
                      color: Color(0xffB6BDBC),
                      fontSize: 18),
                )),
            Positioned(
              top: queryData.size.height - BgSize.top,
              right: queryData.size.width - BgSize.right,
              left: queryData.size.width - BgSize.left,
              child: Container(
                height: 350,
                decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage("assets/bg.png"))),
              ),
            ),
            Positioned(
              top: queryData.size.height - LogoSize.top,
              left: queryData.size.width - LogoSize.left,
              right: queryData.size.width - LogoSize.right,
              child: Container(
                height: 100,
                decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage("assets/nu.png"))),
              ),
            ),
            Positioned(
                width: 150.0,
                height: 43.0,
                top: queryData.size.height - BtnSize.top,
                left: queryData.size.width - BtnSize.left,
                child: RaisedButton(
                  color: Color(0xffF1C552),
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  onPressed: () {
                    context
                        .bloc<PageBloc>()
                        .add(GoToMainPage(pageIndex: 0, pageName: "Berita"));
                  },
                  child: Text(StringConst.APP_START,
                      style: TextStyle(
                          fontFamily: StringConst.SEMI_FONT_NAME,
                          fontSize: 18)),
                )),
          ],
        ),
      ),
    );
  }
}
