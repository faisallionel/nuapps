part of 'pages.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PageBloc, PageState>(
        builder: (_, pageState) => (pageState is OnSplashPage)
            ? SplashPage()
            : (pageState is OnNewsDetailPage)
                ? NewsDetailPage(pageState.news)
                : (pageState is OnQuranDetailPage)
                    ? QuranDetailPage(pageState.quran, pageState.detail)
                    : (pageState is OnQuranPage)
                        ? QuranWidgets()
                        : (pageState is OnMainPage)
                            ? MainPage(pageState.pageIndex, pageState.pageName)
                            : (pageState is OnTahlilPage)
                                ? TahlilWidgets()
                                : SplashPage());
  }
}
