part of 'widgets.dart';

class DoaWidgets extends StatefulWidget {
  @override
  _DoaWidgetsState createState() => _DoaWidgetsState();
}

class _DoaWidgetsState extends State<DoaWidgets> {
  Quran quran;

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    (context as Element).reassemble();
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    (context as Element).reassemble();
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);

    return Container(
        padding: EdgeInsets.symmetric(vertical: queryData.size.height * .005),
        height: queryData.size.height * 1,
        child: BlocBuilder<DoaBloc, DoaState>(builder: (_, pageState) {
          if (pageState is DoaLoaded) {
            Doa data = pageState.doa;
            // return Text("faisal");
            return SmartRefresher(
              enablePullDown: true,
              enablePullUp: false,
              header: WaterDropMaterialHeader(
                color: Colors.white,
                backgroundColor: Hexcolor("#038175"),
              ),
              controller: _refreshController,
              onRefresh: _onRefresh,
              onLoading: _onLoading,
              child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: data.data.length,
                  itemBuilder: (_, index) {
                    return DoaCards(
                      data: data.data[index],
                      index: index,
                    );
                  }),
            );
          } else {
            return SpinKitFadingCircle(
              color: mainColor,
              size: queryData.size.width * .5,
            );
          }
        }));
  }
}

class DoaCards extends StatelessWidget {
  final DataDoa data;
  final int index;

  DoaCards({this.data, this.index});

  @override
  // ignore: missing_return
  Widget build(BuildContext context) {
    return ExpansionTile(
      leading: Container(
        width: SizeConfig.safeBlockHorizontal * 10,
        height: SizeConfig.safeBlockHorizontal * 10,
        decoration: BoxDecoration(
          color: Hexcolor("#038175"),
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
        child: Padding(
          padding: EdgeInsets.fromLTRB(
              SizeConfig.safeBlockHorizontal * 3.5,
              SizeConfig.safeBlockHorizontal * 2.5,
              SizeConfig.safeBlockHorizontal * 0,
              SizeConfig.safeBlockHorizontal * 0),
          child: Text(
            (index + 1).toString(),
            overflow: TextOverflow.ellipsis,
            style: GoogleFonts.roboto().copyWith(
              color: Colors.white,
              fontSize: 15,
              fontWeight: FontWeight.normal,
            ),
          ),
        ),
      ),
      title: Row(
        children: <Widget>[
          Expanded(
              flex: 1,
              child: Text(
                data.namaDoa,
                style: new TextStyle(
                    fontFamily: ArabicFonts.Cairo,
                    package: 'google_fonts_arabic',
                    fontSize: SizeConfig.safeBlockHorizontal * 3,
                    color: Hexcolor("#034f81")),
                textAlign: TextAlign.right,
                overflow: TextOverflow.visible,
              )),
        ],
      ),
      subtitle: Row(
        children: <Widget>[
          Expanded(
              flex: 1,
              child: Column(
                children: <Widget>[
                  Html(
                    style: {
                      "p": Style(
                          fontFamily: ArabicFonts.Scheherazade,
                          fontSize:
                              FontSize(SizeConfig.safeBlockHorizontal * 6),
                          color: Hexcolor("#038175"),
                          textAlign: TextAlign.right),
                    },
                    // linkStyle: TextStyle(
                    //   fontFamily: ArabicFonts.Scheherazade,
                    //   fontSize: SizeConfig.safeBlockHorizontal * 6,
                    //   color: Hexcolor("#038175"),
                    // ),
                    data: data.arabText,
                    // // ignore: missing_return
                    // customTextAlign: (dom.Node node) {
                    //   if (node is dom.Element) {
                    //     switch (node.localName) {
                    //       case "p":
                    //         return TextAlign.right;
                    //     }
                    //   }
                    // },
                  ),
                  Html(
                    style: {
                      "p": Style(
                          fontFamily: ArabicFonts.Scheherazade,
                          fontSize:
                              FontSize(SizeConfig.safeBlockHorizontal * 4),
                          color: Colors.black,
                          textAlign: TextAlign.left),
                    },
                    data: data.bacaText,
                    // linkStyle: TextStyle(
                    //   fontFamily: ArabicFonts.Scheherazade,
                    //   fontSize: SizeConfig.safeBlockHorizontal * 4,
                    //   color: Hexcolor("#038175"),
                    // ),
                    // // ignore: missing_return
                    // customTextAlign: (dom.Node node) {
                    //   if (node is dom.Element) {
                    //     switch (node.localName) {
                    //       case "p":
                    //         return TextAlign.left;
                    //     }
                    //   }
                    // },
                  ),
                ],
              )),
        ],
      ),
      trailing: Opacity(opacity: 0),
      children: [
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.safeBlockHorizontal * 10),
          child: Html(
            style: {
              "p": Style(
                  fontFamily: ArabicFonts.Scheherazade,
                  fontSize: FontSize(SizeConfig.safeBlockHorizontal * 4),
                  color: Colors.black,
                  textAlign: TextAlign.justify),
            },
            // linkStyle: TextStyle(
            //   fontFamily: ArabicFonts.Scheherazade,
            //   fontSize: SizeConfig.safeBlockHorizontal * 4,
            //   color: Colors.black,
            // ),
            data: data.indoText,
            // customTextAlign: (dom.Node node) {
            //   if (node is dom.Element) {
            //     switch (node.localName) {
            //       case "p":
            //         return TextAlign.justify;
            //         break;
            //     }
            //   }
            // },
          ),
        ),
      ],
    );
  }
}
