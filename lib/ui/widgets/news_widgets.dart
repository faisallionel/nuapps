part of 'widgets.dart';

class NewsWidgets extends StatefulWidget {
  @override
  _NewsWidgetsState createState() => _NewsWidgetsState();
}

class _NewsWidgetsState extends State<NewsWidgets> {
  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);

    // return SafeArea(
    //   child: Column(
    //     children: <Widget>[
    //       Expanded(
    //         flex: 2,
    //         child: Container(
    //           width: queryData.size.width * 1,
    //           margin: EdgeInsets.only(
    //               left: queryData.size.width * .050,
    //               top: queryData.size.height * .008),
    //           child: Text(
    //             StringConst.RECENT_NEWS,
    //             style: TextStyle(
    //                 fontFamily: StringConst.BOLD_FONT_NAME,
    //                 color: Color(0xff038175),
    //                 fontSize: 18),
    //           ),
    //         ),
    //       ),
    //       Expanded(
    //         child: HorizontalWidgets(),
    //         flex: 11,
    //       ),
    //       Expanded(
    //         flex: 2,
    //         child: Container(
    //           width: queryData.size.width * 1,
    //           margin: EdgeInsets.only(
    //               left: queryData.size.width * .050,
    //               top: queryData.size.height * .008),
    //           child: Text(
    //             StringConst.ALL_NEWS,
    //             textAlign: TextAlign.left,
    //             style: TextStyle(
    //                 fontFamily: StringConst.BOLD_FONT_NAME,
    //                 color: Color(0xff038175),
    //                 fontWeight: FontWeight.bold,
    //                 fontSize: 18),
    //           ),
    //         ),
    //       ),
    //       Expanded(
    //         child: VerticalWidgets(),
    //         flex: 16,
    //       ),

    //       // DUMMY DATA

    //       // SizedBox(
    //       //   height: queryData.size.height * .020,
    //       // ),
    //       // Container(
    //       //   width: queryData.size.width * 1,
    //       //   margin: EdgeInsets.only(left: 10.0),
    //       //   child: Text(
    //       //     "Berita Terbaru",
    //       //     style: blackTextFont,
    //       //   ),
    //       // ),
    //       // HorizontalWidgets(),
    //       // Container(
    //       //   width: queryData.size.width * 1,
    //       //   margin: EdgeInsets.only(left: 10.0),
    //       //   child: Text(
    //       //     "Semua Berita",
    //       //     textAlign: TextAlign.left,
    //       //     style: GoogleFonts.roboto().copyWith(
    //       //         color: Hexcolor("#038175"),
    //       //         fontWeight: FontWeight.bold,
    //       //         fontSize: 20),
    //       //   ),
    //       // ),
    //       // VerticalWidgets(),
    //     ],
    //   ),
    // );
    return SafeArea(
        child: Column(
      children: [
        Expanded(
          flex: 2,
          child: Container(
            width: queryData.size.width * 1,
            margin: EdgeInsets.only(
                left: queryData.size.width * .050,
                top: queryData.size.height * .008),
            child: Text(
              StringConst.RECENT_NEWS,
              style: TextStyle(
                  fontFamily: StringConst.BOLD_FONT_NAME,
                  color: Color(0xff038175),
                  fontSize: 18),
            ),
          ),
        ),
        Expanded(
          child: HorizontalWidgets(),
          flex: 11,
        ),
        Expanded(
          flex: 2,
          child: Container(
            width: queryData.size.width * 1,
            margin: EdgeInsets.only(
                left: queryData.size.width * .050,
                top: queryData.size.height * .008),
            child: Text(
              StringConst.ALL_NEWS,
              textAlign: TextAlign.left,
              style: TextStyle(
                  fontFamily: StringConst.BOLD_FONT_NAME,
                  color: Color(0xff038175),
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
          ),
        ),
        Expanded(
          child: VerticalWidgets(),
          flex: 16,
        ),
      ],
    ));
  }
}

class HorizontalWidgets extends StatelessWidget {
  final EdgeInsets margin;
  final double height;

  HorizontalWidgets({this.margin, this.height});

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);

    return Container(
      padding: EdgeInsets.symmetric(vertical: queryData.size.height * .005),
      height: queryData.size.height * .270,
      child: BlocBuilder<NewsBloc, NewsState>(builder: (_, newsState) {
        if (newsState is NewsLoaded) {
          List<News> news = newsState.news
              .sublist((newsState.news.length - 3), newsState.news.length);
          return ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: news.length,
              itemBuilder: (_, index) {
                return CardComponents(
                  horizontal: true,
                  title: news[index].title,
                  date: news[index].publishDateText,
                  image: NetworkImage(
                    news[index].picture['16_9'].thumb.toString(),
                  ),
                  newsData: news[index],
                );
              });
        } else {
          return SpinKitFadingCircle(
            color: mainColor,
            size: queryData.size.width * .5,
          );
        }
      }),
    );
  }
}

class VerticalWidgets extends StatefulWidget {
  @override
  _VerticalWidgetsState createState() => _VerticalWidgetsState();
}

class _VerticalWidgetsState extends State<VerticalWidgets> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    (context as Element).reassemble();
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    (context as Element).reassemble();
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);

    return Container(
        margin: EdgeInsets.symmetric(vertical: queryData.size.height * .010),
        child: BlocBuilder<NewsBloc, NewsState>(builder: (_, newsState) {
          if (newsState is NewsLoaded) {
            List<News> news = newsState.news;
            return ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: news.length,
                itemBuilder: (_, index) {
                  return CardComponents(
                    horizontal: false,
                    title: news[index].title,
                    date: news[index].publishDateText,
                    detail: true,
                    image: NetworkImage(
                        news[index].picture['4_3'].thumb.toString()),
                    preview: news[index].preview,
                    newsData: news[index],
                  );
                });
          } else {
            return SpinKitFadingCircle(
              color: mainColor,
              size: queryData.size.width * .5,
            );
          }
        }));

    // return Container(
    //   margin: EdgeInsets.symmetric(vertical: 20.0),
    //   height: queryData.size.height * 0.3622,
    //   child: ListView(
    //     scrollDirection: Axis.vertical,
    //     children: <Widget>[
    //       SizedBox(
    //         width: queryData.size.height * .050,
    //       ),
    //       CardComponents(
    //         title: "Amal dari Rumah Bersama NU",
    //         date: "13 Agustus 2020",
    //       ),
    //       SizedBox(
    //         width: queryData.size.height * .010,
    //       ),
    //       CardComponents(
    //         title: "Amal dari Rumah Bersama NU",
    //         date: "13 Agustus 2020",
    //       ),
    //       SizedBox(
    //         width: queryData.size.height * .010,
    //       ),
    //       CardComponents(),
    //       SizedBox(
    //         width: queryData.size.height * .010,
    //       ),
    //     ],
    //   ),
    // );
  }
}

class CardComponents extends StatelessWidget {
  final int index;
  final String title;
  final String preview;
  final String date;
  final bool horizontal;
  final NetworkImage image;
  final bool detail;
  final News newsData;

  CardComponents(
      {this.index,
      this.title,
      this.date,
      this.horizontal = false,
      this.image,
      this.detail = false,
      this.preview,
      this.newsData});

  @override
  // ignore: missing_return
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    // if (this.detail == false) {
    //   return Wrap(children: <Widget>[
    //     (horizontal && horizontal != null)
    //         ? SizedBox(
    //             width: queryData.size.width * .030,
    //           )
    //         : SizedBox(
    //             height: queryData.size.height * .280,
    //             width: 10.0,
    //           ),
    //     Container(
    //       width: queryData.size.width * .950,
    //       child: InkWell(
    //         borderRadius: BorderRadius.all(Radius.circular(10.0)),
    //         splashColor: Hexcolor("#038175"),
    //         onTap: () {
    //           // Navigator.push(
    //           //     context,
    //           //     new MaterialPageRoute(
    //           //         builder: (context) => NewsDetailPage(
    //           //               newsData: this.newsData,
    //           //             )));
    //           context.bloc<PageBloc>().add(GoToNewsDetailPage(this.newsData));
    //         },
    //         child: Card(
    //           shape: RoundedRectangleBorder(
    //             borderRadius: BorderRadius.circular(15.0),
    //           ),
    //           semanticContainer: true,
    //           clipBehavior: Clip.antiAliasWithSaveLayer,
    //           elevation: 2,
    //           child: Container(
    //             height: 200,
    //             decoration: BoxDecoration(
    //                 image: DecorationImage(
    //                     image: image,
    //                     fit: BoxFit.fill,
    //                     alignment: Alignment.center)),
    //             child: Padding(
    //               padding: EdgeInsets.all(10.0),
    //               child: Stack(
    //                 children: <Widget>[
    //                   Flexible(
    //                     child: Positioned(
    //                         child: Container(
    //                           width: queryData.size.width * .920,
    //                           child: Text(
    //                             "$title",
    //                             overflow: TextOverflow.ellipsis,
    //                             style: GoogleFonts.roboto().copyWith(
    //                                 color: Colors.white,
    //                                 fontSize: 18,
    //                                 fontWeight: FontWeight.bold,
    //                                 shadows: [
    //                                   Shadow(
    //                                       blurRadius: 30.0, color: Colors.black)
    //                                 ]),
    //                           ),
    //                         ),
    //                         top: queryData.size.height * 0.150),
    //                   ),
    //                   Positioned(
    //                     child: Text(
    //                       "$date",
    //                       style: GoogleFonts.roboto().copyWith(
    //                           color: Colors.white,
    //                           fontSize: 14,
    //                           fontWeight: FontWeight.normal,
    //                           shadows: [
    //                             Shadow(blurRadius: 30.0, color: Colors.black)
    //                           ]),
    //                     ),
    //                     top: queryData.size.height * 0.200,
    //                   )
    //                 ],
    //               ),
    //             ),
    //           ),
    //         ),
    //       ),
    //     ),
    //     (horizontal && horizontal != null)
    //         ? SizedBox(
    //             width: queryData.size.width * .030,
    //           )
    //         : Container()
    //   ]);
    // } else if (this.detail == true) {
    //   // detail halaman
    //   return Wrap(children: <Widget>[
    //     (horizontal && horizontal != null)
    //         ? SizedBox(
    //             width: queryData.size.width * .030,
    //           )
    //         : SizedBox(
    //             height: queryData.size.height * .200,
    //             width: 10.0,
    //           ),
    //     Container(
    //       width: queryData.size.width * .950,
    //       child: InkWell(
    //         borderRadius: BorderRadius.all(Radius.circular(10.0)),
    //         splashColor: Hexcolor("#038175"),
    //         onTap: () {
    //           // print('Card tapped. $title');
    //           // Navigator.push(
    //           //     context,
    //           //     new MaterialPageRoute(
    //           //         builder: (context) => NewsDetailPage(

    //           //               newsData: this.newsData,
    //           //             )));
    //           context.bloc<PageBloc>().add(GoToNewsDetailPage(this.newsData));
    //         },
    //         child: Card(
    //           // shape: RoundedRectangleBorder(
    //           //   borderRadius: BorderRadius.circular(15.0),
    //           // ),
    //           semanticContainer: true,
    //           clipBehavior: Clip.antiAliasWithSaveLayer,
    //           elevation: 2,
    //           child: Container(
    //             height: queryData.size.height * .180,
    //             child: Row(
    //               children: <Widget>[
    //                 Expanded(
    //                     flex: 1,
    //                     child: Container(
    //                         height: queryData.size.height * .250,
    //                         decoration: BoxDecoration(
    //                             image: DecorationImage(
    //                                 image: image, fit: BoxFit.fitHeight)),
    //                         child: null)),
    //                 Expanded(
    //                   flex: 1,
    //                   child: Wrap(
    //                     direction: Axis.vertical,
    //                     children: <Widget>[
    //                       Padding(
    //                         padding: EdgeInsets.only(
    //                             left: queryData.size.width * .020),
    //                         child: Flexible(
    //                           child: Container(
    //                             width: 150,
    //                             height: 50,
    //                             child: Text(
    //                               "$title",
    //                               overflow: TextOverflow.visible,
    //                               style: GoogleFonts.roboto().copyWith(
    //                                 color: Hexcolor("#B6BDBC"),
    //                                 fontSize: 15,
    //                                 fontWeight: FontWeight.normal,
    //                               ),
    //                             ),
    //                           ),
    //                         ),
    //                       ),
    //                     ],
    //                   ),
    //                 ),
    //               ],
    //             ),
    //           ),
    //         ),
    //       ),
    //     ),
    //     (horizontal && horizontal != null)
    //         ? SizedBox(
    //             width: queryData.size.width * .030,
    //           )
    //         : Container()
    //   ]);
    // }
    if (this.detail == false) {
      return Wrap(
        children: [
          (horizontal && horizontal != null)
              ? SizedBox(
                  width: queryData.size.width * .030,
                )
              : SizedBox(
                  height: queryData.size.height * .280,
                  width: 10.0,
                ),
          Container(
              width: queryData.size.width * .950,
              child: InkWell(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                splashColor: Hexcolor("#038175"),
                onTap: () {
                  context
                      .bloc<PageBloc>()
                      .add(GoToNewsDetailPage(this.newsData));
                },
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  semanticContainer: true,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  elevation: 2,
                  child: Container(
                    height: 200,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: image,
                            fit: BoxFit.fill,
                            alignment: Alignment.center)),
                    child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Stack(
                          fit: StackFit.expand,
                          children: [
                            Positioned(
                                child: Container(
                                  width: queryData.size.width * .920,
                                  child: Text(
                                    "$title",
                                    overflow: TextOverflow.ellipsis,
                                    style: GoogleFonts.roboto().copyWith(
                                        color: Colors.white,
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        shadows: [
                                          Shadow(
                                              blurRadius: 30.0,
                                              color: Colors.black)
                                        ]),
                                  ),
                                ),
                                top: queryData.size.height * 0.150),
                            Positioned(
                              child: Text(
                                "$date",
                                style: GoogleFonts.roboto().copyWith(
                                    color: Colors.white,
                                    fontSize: 14,
                                    fontWeight: FontWeight.normal,
                                    shadows: [
                                      Shadow(
                                          blurRadius: 30.0, color: Colors.black)
                                    ]),
                              ),
                              top: queryData.size.height * 0.200,
                            )
                          ],
                        )),
                  ),
                ),
              )),
          (horizontal && horizontal != null)
              ? SizedBox(
                  width: queryData.size.width * .030,
                )
              : Container()
        ],
      );
    } else if (this.detail == true) {
      return Wrap(
        children: [
          (horizontal && horizontal != null)
              ? SizedBox(
                  width: queryData.size.width * .030,
                )
              : SizedBox(
                  height: queryData.size.height * .200,
                  width: 10.0,
                ),
          Container(
            width: queryData.size.width * .950,
            child: InkWell(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                splashColor: Hexcolor("#038175"),
                onTap: () {
                  context
                      .bloc<PageBloc>()
                      .add(GoToNewsDetailPage(this.newsData));
                },
                child: Card(
                  semanticContainer: true,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  elevation: 2,
                  child: Container(
                    height: queryData.size.height * .180,
                    child: Row(
                      children: [
                        Expanded(
                            flex: 1,
                            child: Container(
                                height: queryData.size.height * .250,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: image, fit: BoxFit.fitHeight)),
                                child: null)),
                        Expanded(
                          flex: 1,
                          child: Text(
                            "$title",
                            overflow: TextOverflow.visible,
                            style: GoogleFonts.roboto().copyWith(
                              color: Hexcolor("#B6BDBC"),
                              fontSize: 15,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )),
          ),
          (horizontal && horizontal != null)
              ? SizedBox(
                  width: queryData.size.width * .030,
                )
              : Container()
        ],
      );
    }
  }
}

class GifHeader1 extends RefreshIndicator {
  GifHeader1() : super(height: 80.0, refreshStyle: RefreshStyle.Follow);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return GifHeader1State();
  }
}

class GifHeader1State extends RefreshIndicatorState<GifHeader1>
    with SingleTickerProviderStateMixin {
  GifController _gifController;

  @override
  void initState() {
    // TODO: implement initState
    // init frame is 2
    _gifController = GifController(
      vsync: this,
      value: 1,
    );
    super.initState();
  }

  @override
  void onModeChange(RefreshStatus mode) {
    // TODO: implement onModeChange
    if (mode == RefreshStatus.refreshing) {
      _gifController.repeat(
          min: 0, max: 30, period: Duration(milliseconds: 500));
    }
    super.onModeChange(mode);
  }

  @override
  Future<void> endRefresh() {
    // TODO: implement endRefresh
    // _gifController.value = 29;
    return _gifController.animateTo(59, duration: Duration(milliseconds: 500));
  }

  @override
  void resetValue() {
    // TODO: implement resetValue
    // reset not ok , the plugin need to update lowwer
    _gifController.value = 0;
    super.resetValue();
  }

  @override
  Widget buildContent(BuildContext context, RefreshStatus mode) {
    // TODO: implement buildContent
    return GifImage(
      image: AssetImage("assets/loader/Dual Ball-1s-200px.gif"),
      controller: _gifController,
      height: 80.0,
      width: 537.0,
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _gifController.dispose();
    super.dispose();
  }
}

class GifFooter1 extends StatefulWidget {
  GifFooter1() : super();

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _GifFooter1State();
  }
}

class _GifFooter1State extends State<GifFooter1>
    with SingleTickerProviderStateMixin {
  GifController _gifController;

  @override
  void initState() {
    // TODO: implement initState
    // init frame is 2
    _gifController = GifController(
      vsync: this,
      value: 1,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CustomFooter(
      height: 80,
      builder: (context, mode) {
        return GifImage(
          image: AssetImage("images/gifindicator1.gif"),
          controller: _gifController,
          height: 80.0,
          width: 537.0,
        );
      },
      loadStyle: LoadStyle.ShowWhenLoading,
      onModeChange: (mode) {
        if (mode == LoadStatus.loading) {
          _gifController.repeat(
              min: 0, max: 29, period: Duration(milliseconds: 500));
        }
      },
      endLoading: () async {
        _gifController.value = 29;
        return _gifController.animateTo(59,
            duration: Duration(milliseconds: 500));
      },
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _gifController.dispose();
    super.dispose();
  }
}
