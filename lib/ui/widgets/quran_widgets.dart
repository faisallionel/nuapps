part of 'widgets.dart';

class QuranWidgets extends StatefulWidget {
  @override
  _QuranWidgetsState createState() => _QuranWidgetsState();
}

class _QuranWidgetsState extends State<QuranWidgets> {
  Quran quran;

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    (context as Element).reassemble();
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    (context as Element).reassemble();
  }

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);

    return Container(
        padding: EdgeInsets.symmetric(vertical: queryData.size.height * .005),
        height: queryData.size.height * 1,
        child: BlocBuilder<QuranBloc, QuranState>(builder: (_, pageState) {
          if (pageState is QuranLoaded) {
            Quran quran = pageState.quran;
            return SmartRefresher(
              enablePullDown: true,
              enablePullUp: false,
              header: WaterDropMaterialHeader(
                color: Colors.white,
                backgroundColor: Hexcolor("#038175"),
              ),
              controller: _refreshController,
              onRefresh: _onRefresh,
              onLoading: _onLoading,
              child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: quran.data.length,
                  itemBuilder: (_, index) {
                    // return print(listQuran[index].toJson());
                    // return Text(listQuran[index].nameLatin);
                    return QuranCards(
                        data: quran.data[index], index: index, quran: quran);
                  }),
            );
          } else {
            return Center(
                child: CircularProgressIndicator(
              backgroundColor: Colors.yellow,
            ));
          }
        }));
  }
}

class QuranCards extends StatelessWidget {
  final DataQuran data;
  final Quran quran;
  final int index;

  QuranCards({this.data, this.index, this.quran});

  @override
  // ignore: missing_return
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    SizeConfig().init(context);
    return Wrap(children: <Widget>[
      // index == 0
      //     ? Padding(
      //         padding: EdgeInsets.all(SizeConfig.screenWidth * .050),
      //         child: TextFormField(
      //           decoration: new InputDecoration(
      //             labelText: "Search",
      //             fillColor: Colors.white,
      //             border: new OutlineInputBorder(
      //               borderRadius: new BorderRadius.circular(10.0),
      //               borderSide: new BorderSide(),
      //             ),
      //             //fillColor: Colors.green
      //           ),
      //         ))
      //     : Container(),
      Padding(
        padding: EdgeInsets.fromLTRB(queryData.size.width * .040, 0, 0, 0),
        child: Container(
          width: queryData.size.width * .900,
          height: queryData.size.height * .150,
          child: InkWell(
            onTap: () {
              context
                  .bloc<PageBloc>()
                  .add(GoToQuranDetailPage(null, this.data));
            },
            child: Container(
              height: queryData.size.height * .200,
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          color: Colors.grey,
                          width: SizeConfig.safeBlockVertical * .2))),
              child: Row(
                children: <Widget>[
                  Expanded(
                      flex: 1,
                      child: SizedBox.expand(
                          child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: SizeConfig.safeBlockHorizontal,
                            vertical: SizeConfig.safeBlockVertical * 5),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Hexcolor("#038175"),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                          ),
                          child: Center(
                            child: Text(
                              data.id.toString(),
                              overflow: TextOverflow.visible,
                              style: GoogleFonts.roboto().copyWith(
                                color: Hexcolor("#B6BDBC"),
                                fontSize: 15,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                        ),
                      ))),
                  Expanded(
                      flex: 5,
                      child: SizedBox.expand(
                        child: Padding(
                          padding: EdgeInsets.only(
                              top: SizeConfig.safeBlockVertical * 5,
                              bottom: SizeConfig.safeBlockVertical * 3,
                              left: SizeConfig.safeBlockHorizontal * 5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Expanded(
                                flex: 1,
                                child: Container(
                                  child: Text(data.nameLatin,
                                      overflow: TextOverflow.visible,
                                      style: blackTextFont.copyWith(
                                          fontSize:
                                              SizeConfig.safeBlockHorizontal *
                                                  6)),
                                ),
                              ),
                              Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: Text(data.name,
                                        overflow: TextOverflow.visible,
                                        style: blackTextFont.copyWith(
                                            fontSize:
                                                SizeConfig.safeBlockHorizontal *
                                                    4)),
                                  ))
                            ],
                          ),
                        ),
                      )),
                ],
              ),
            ),
          ),
        ),
      ),
    ]);
  }
}
