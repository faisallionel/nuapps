part of 'widgets.dart';

class TahlilWidgets extends StatelessWidget {
  Quran quran;

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    void _onRefresh() async {
      // monitor network fetch
      await Future.delayed(Duration(milliseconds: 1000));
      // if failed,use refreshFailed()
      (context as Element).reassemble();
      _refreshController.refreshCompleted();
    }

    void _onLoading() async {
      // monitor network fetch
      await Future.delayed(Duration(milliseconds: 1000));
      (context as Element).reassemble();
    }

    MediaQueryData queryData;
    queryData = MediaQuery.of(context);

    return Container(
        margin: EdgeInsets.symmetric(vertical: queryData.size.height * .010),
        child: BlocBuilder<TahlilBloc, TahlilState>(builder: (_, tahlilState) {
          if (tahlilState is TahlilLoaded) {
            Tahlil data = tahlilState.tahlil;
            return SmartRefresher(
              enablePullDown: true,
              enablePullUp: false,
              header: WaterDropMaterialHeader(
                color: Colors.white,
                backgroundColor: Hexcolor("#038175"),
              ),
              controller: _refreshController,
              onRefresh: _onRefresh,
              onLoading: _onLoading,
              child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: data.data.length,
                  itemBuilder: (_, index) {
                    // return print(listQuran[index].toJson());
                    // return Text(listQuran[index].nameLatin);
                    return TahlilCards(
                      data: data.data[index],
                      index: index,
                    );
                  }),
            );
          } else {
            return SpinKitFadingCircle(
              color: mainColor,
              size: queryData.size.width * .5,
            );
          }
        }));
  }
}

class TahlilCards extends StatelessWidget {
  final DataTahlil data;
  final int index;

  TahlilCards({this.data, this.index});

  @override
  // ignore: missing_return
  Widget build(BuildContext context) {
    return ExpansionTile(
      leading: Container(
        width: SizeConfig.safeBlockHorizontal * 10,
        height: SizeConfig.safeBlockHorizontal * 10,
        decoration: BoxDecoration(
          color: Hexcolor("#038175"),
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
        child: Padding(
          padding: EdgeInsets.fromLTRB(
              SizeConfig.safeBlockHorizontal * 3.5,
              SizeConfig.safeBlockHorizontal * 2.5,
              SizeConfig.safeBlockHorizontal * 0,
              SizeConfig.safeBlockHorizontal * 0),
          child: Text(
            (index + 1).toString(),
            overflow: TextOverflow.ellipsis,
            style: GoogleFonts.roboto().copyWith(
              color: Colors.white,
              fontSize: 15,
              fontWeight: FontWeight.normal,
            ),
          ),
        ),
      ),
      title: Wrap(
        crossAxisAlignment: WrapCrossAlignment.end,
        alignment: WrapAlignment.end,
        children: [
          Html(
            style: {
              "p": Style(
                  fontFamily: ArabicFonts.Scheherazade,
                  fontSize: FontSize(SizeConfig.safeBlockHorizontal * 6),
                  color: Hexcolor("#038175"),
                  textAlign: TextAlign.right),
            },
            data: data.arabText,
            // linkStyle: TextStyle(
            //     fontFamily: ArabicFonts.Scheherazade,
            //     package: 'google_fonts_arabic',
            //     fontSize: SizeConfig.safeBlockHorizontal * 8,
            //     color: Hexcolor("#038175")),
            // customTextAlign: (dom.Node node) {
            //   if (node is dom.Element) {
            //     switch (node.localName) {
            //       case "p":
            //         return TextAlign.right;
            //     }
            //   }
            // },
          ),
        ],
      ),
      subtitle: Html(
        style: {
          "p": Style(
              fontFamily: ArabicFonts.Scheherazade,
              fontSize: FontSize(SizeConfig.safeBlockHorizontal * 4),
              color: Colors.black,
              textAlign: TextAlign.left),
        },
        data: data.indoText,
        // linkStyle: TextStyle(
        //   fontFamily: ArabicFonts.Scheherazade,
        //   fontSize: SizeConfig.safeBlockHorizontal * 4,
        //   color: Colors.black,
        // ),
        // customTextAlign: (dom.Node node) {
        //   if (node is dom.Element) {
        //     switch (node.localName) {
        //       case "p":
        //         return TextAlign.left;
        //     }
        //   }
        // },
      ),
      trailing: Opacity(opacity: 0),
    );
  }
}
